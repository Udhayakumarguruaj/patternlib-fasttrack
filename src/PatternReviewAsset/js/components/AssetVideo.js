import React from 'react';
import Video from 'react-html5video';
const flowplayerVideo = { width: 600,
						height: 338 };

const AssetVideo = (props) => { 

	return(<div className="pe-input pe-input--horizontal">
		<div style={flowplayerVideo} id="reviewAssestVideo">
		<Video controls autoPlay>
            <source src={props.url} type="video/mp4" />
        </Video>
		</div></div>)
}

AssetVideo.propTypes = {
	url:React.PropTypes.string
}

export default AssetVideo;
