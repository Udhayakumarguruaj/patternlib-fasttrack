/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 * @class EntryPopup component used to create a Pop Up to the 
 * Add Asset page
 * @author Udhayakumar Gururaj
 **/
import React from 'react';
import AddAnAsset from './AddAnAsset'
import Modal from 'react-modal';
import { Link, browserHistory, hashHistory } from 'react-router';
import { Scrollbars } from 'react-custom-scrollbars';

class EntryPopup extends React.Component{
	/**
	* @constructor is used to create a Object for EntryPopup Component
	* and also it defines the state for the EntryPopup component
	* @param {function} closeModal
	*/
	constructor(props){
		super(props);
		this.state={
			modalIsOpen : true
		}
		this.closeModal = this.closeModal.bind(this);
		sessionStorage.currentTab = 0;
		hashHistory.push('/');
	}
	
	
	/**
	* @function closeModal method will be used to change the state of the 
	* EntryPopup component
	*/
	closeModal(){
		if(this.props.children == null){
			this.setState({modalIsOpen : false});
		}else{
			if(this.props.children.props.location.pathname == '/ReviewAsset'){
	             hashHistory.push('/');
			}	
		}
		
	
	}
	/**
	* @default render will be used for returning the Pop up DOM and 
	* close button functionality
	*/
	render(){
		return(
			<div>
				
				<Modal isOpen = {this.state.modalIsOpen}>
				<Scrollbars>
					<div className='row modalHeadDiv'>
						<div className='pageTitle'>Add an Asset</div>
						<div className='closeButtonDiv'>
						<i className='fa fa-times' aria-hidden='true' 
						onClick={this.closeModal} ></i></div>
						</div>
					<div className='modalBodyDiv'>
		          		<AddAnAsset children={this.props.children} />
		          	</div>
		          	</Scrollbars>
	        	</Modal>
	        	 
			</div>

			)
	}
}

EntryPopup.propTypes = {
	children : React.PropTypes.object
}
export default EntryPopup;
