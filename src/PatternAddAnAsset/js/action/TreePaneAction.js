/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 * @class TreePaneAction
 * It will be used to create a action object / middleware object will be return
 * to called function and then reducers will be called
 * @author TDC
 **/
import assetsApi from '../api/TreePaneApi'
import fileUploadApi from '../api/fileUploadApi'
import { GET_FOLDER, TOGGLE,SET_REF} from '../constants/TreePaneConstant'
import {fetchingAssets} from '../action/assets';
import {getNodeRef,
        getFirstObj,
        highlightChildren,
        getFirstName,
        getNodeRefValue} from '../../../common/components/browseAssetUtil';
import JsonData from '../components/folderpane/TreeNodeUtil';
import {DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS} from '../constants/paginationConstants';
import serviceUrls from '../constants/service';

let nodeRef;
/**
* @function getFolders method is used for get the all the folder from
* alfresco and dispatch to the reducers
*/
  export function  getFolders(arg='browseasset'){
    return dispatch => {

      fileUploadApi.get_token().then(function (success){
        let token = JSON.parse(success.text).data.ticket;

      assetsApi.getRootChildren(token).then(function (res){

        if(res.body !== undefined && res.body.results.length > 0){
           let treeFolder = JsonData.getCustomFolder(res.body);
           treeFolder.show = false;
           highlightChildren(treeFolder, getFirstName(treeFolder));
           dispatch({
             type : GET_FOLDER,
             data : treeFolder,
           })
           nodeRef = getNodeRef(getNodeRefValue(getFirstObj(treeFolder)));
           if(arg == 'browseasset'){
           	dispatch(fetchingAssets(nodeRef, DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS, serviceUrls.allFilter));	
           }else{
          	 let curFolder = _.filter(treeFolder)[0]['nodeRef'].split('/')[3];
        		   dispatch({
        		   type: SET_REF,
        		   data: curFolder
        		 });
           }
        }else{
          console.log('No Folders found in this search');
        }
      },function (error){
       console.log('fetching child folders data:' + error);
      })

    },function (error){
       console.log('Aflerco_Tocken_Error:' + error);
       console.log(error.message);
     })
  }
  }

/**
* @function getSubFolders method is used for get the all the folder from
* alfresco and dispatch to the reducers
*/
  export function  getSubFolders(arg='browseasset',folderName,child, nodeRef){ 
    return (dispatch,getState) => {

      let _getState = getState().TreePaneReducers;
      return fileUploadApi.get_token().then(function (success){ 
        let token = JSON.parse(success.text).data.ticket;

           assetsApi.getSubFolders(token,folderName,child).then(function (res){ 
		         console.log(res);
             child.items = JsonData.treeNodeUtil(res.body);
             child.items = JsonData.sortFolderTree(child.items);
             child.highlight = true;
             let model = _getState[_getState.length-1];
             model = removeCSSSelector(model, nodeRef);
             if(child.items !== undefined && child.items.length > 0){
                model = getTreeFolder(model, child);
              }else{
                console.log('Folder does not have children');
              }
             model.show = !model.show;
             dispatch({
              type : GET_FOLDER,
              data : model,
          })
		
      },function (error){
       console.log('fetching child folders data:' + error);
      })

    },function (error){
       console.log('Aflerco_Tocken_Error:' + error);
       console.log(error.message);
     })
  }
  }

export function removeCSSSelector(model, nodeRef){ 

  let child;
  for(let obj in model){
    child = model[obj];
    if(child.style !== undefined 
       && child.style.includes('tree-node-selected')
       && child.path !== nodeRef){
           child.style = child.style.replace('tree-node-selected', '');
           model[obj] = child;
      }else{
        if(child.items){
           child.items = removeCSSSelector(child.items, nodeRef);
      }
    }
  }
  return model;
}

export function getTreeFolder(model, child){ 
  let obj;
  for(let treeItem in model){
      obj = model[treeItem];
      if(obj.path === child.path){
        model[treeItem] = child;
        break;
      }else{
        if(obj.items){
          getTreeFolder(obj.items, child);
        }
      }
  }

  return model;
}

/**
* @function toggle method is used for change the state of the 
* component and update it in reducers
* @param {boolean} booValue
* @param {string} folderName
*/
export function toggle(booValue, folderName){

return dispatch => {
      assetsApi.getFoldersService(folderName).then(function (res){
        dispatch({
          type : 'TOGGLE_TEST',
          data : res.body,
          toggle : booValue
        })
      },function (error){
       console.log('fetching child folders data:' + error);
      })
  }
}

/**
* @function setReference method is used for settting reference to the component
* Set reference type will change the state of the component through 
* reducers
* @param {string} nodeRef
*/
export function setReference(nodeRef){
return dispatch => {
      dispatch({
          type : SET_REF,
          data : nodeRef

        }),
      console.log('folder id:' + nodeRef);
  }
}
