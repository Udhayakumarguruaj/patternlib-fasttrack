/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *
 * The assets action makes the api call and passes the data to reducer component
 *
 * @module MediaAssets
 * @file assetsAction
 * @author TDC
 *
*/

import assetsApi from '../api/assets';
import fileUploadApi from '../api/fileUploadApi';
import { DISPLAY_ASSETS, SEND_TO_QUAD} from '../constants/fileUploadConstants';
import {getAssetData} from '../../../common/components/browseAssetUtil';

  let i = 1;
  /** @function fetchMetaData -
 * This method is used for fetching relevant assets data on selecting particular folder.
 * @param {*} noderef - Folder Id of selected folder
 * @param {*} pageNo - The current page number
 * @param {*} maxItems - The maximum items to be returned on current page
 * @param {*} filter - What asset types to be returned
 * @param {*} sortValue - Ascending or descending order based on name and date uploaded
 * @param {string} viewName - The type of view to be displayed(List or Grid) 
 * @returns {function}
 * The object mimicking the metadata object, but with every action wrapped into the 'dispatch' call.
 * This action creator returns a function.
*/
  export function fetchingAssets(nodeRef,pageNo,maxItems,
                              filter, sortValue,viewName){
    let index, limit;
    //let defaultNodeRef = '1a6b62f6-0efb-4448-b405-6c014350191e'; //temporary fix
    if(pageNo===1){
      index = 0;
      limit = index+maxItems;
    }else{
      index = (pageNo*maxItems)-maxItems;
      limit = index+maxItems-1;
    }

    return dispatch => {
      assetsApi.get_assets(nodeRef, index, limit, filter,sortValue).then(function (res){
        // res.body.otherDetails = {
        //   index:index,
        //   limit:limit,
        //   pageNo: pageNo,
        //   pageLimit: maxItems,
        //   showSaveSearch: false,
        //   displayItemCount: maxItems,
        //   viewName: viewName
        // };
        res.body.index = index;
        res.body.limit = limit;
        res.body.pageNo = pageNo;
        res.body.pageLimit = maxItems;
        res.body.showSaveSearch = false;
        res.body.displayItemCount = maxItems;
        if(viewName){
          res.body.viewName = viewName;
        }else{
          res.body.viewName = 'grid-view';
        }
        fileUploadApi.get_token().then(function (success){
          let  token = JSON.parse(success.text).data.ticket;
          res.body.token = token;
          let assetData = getAssetData(res.body);
          dispatch({
            type : DISPLAY_ASSETS,
            data : assetData
          });
        },function (error){
          console.log('fetching assets data:' + error);
        });
      },function (error){
       console.log('fetching assets data:' + error);
      })
  }
}

/** @function selectedRecord -
 * This method is used for sending selected asset data to another page.
 * @param {object} record - Asset data object to be sent to another page
 * @returns {function}
 * The object mimicking the metadata object, but with every action wrapped into the 'dispatch' call.
 * This action creator returns a function.
*/

export function selectedRecord(record){
    return {
        type : SEND_TO_QUAD,
        data : record
    }
}
