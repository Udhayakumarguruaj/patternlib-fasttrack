
import searchLibraryApi from '../api/SearchLibraryApi'
import {AUTO_COMPLETE} from '../constants/searchLibraryConstants';


export function populateAutoComplete(text,savedSearch,lastThreeSearch) {
    return dispatch => {
    	searchLibraryApi.autoComplete_Data(text).then(function (data){
        dispatch({
          type: AUTO_COMPLETE,
          data: data,
          text,
          savedSearch,
          lastThreeSearch
        });
    });

  }
}
