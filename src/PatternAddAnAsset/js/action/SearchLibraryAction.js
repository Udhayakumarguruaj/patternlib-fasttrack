/**
 * Copyright (c) Pearson, Inc.
 * All rights reserved.
 *
 * The Search library action makes api calls for retrieving assets based
 on search value and for saved search operations and passes data
 to relevant reducers
 *
 * @module MediaAssets
 * @file Saved Search Action Manages the actions.
 * @author TDC
 *
*/


import searchLibraryApi from '../api/SearchLibraryApi';
import fileUploadApi from '../api/fileUploadApi';
import { SAVED_SEARCH_VALUE,SEARCH_DISPLAY_ASSETS,UPDATE_CHECKBOX_VALUE,SEARCH_BUTTON_VISIBILITY }
from '../constants/searchLibraryConstants';
import { fetchSavedSearchData, checkBoxHandler } from '../action/savedSearchAction';
const localforage = require('localforage');
import {DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS} from '../constants/paginationConstants';
import {AUTO_COMPLETE} from '../constants/searchLibraryConstants';
import serviceUrls from '../constants/service';


/** @function getSearchProductItems -
 * This method is used for fetching relevant assets data on searching a value.
 * @param {string} value - Value to be searched
 * @param {*} pageNo - The current page number
 * @param {*} maxItems - The maximum items to be returned on current page
 * @param {*} filter - What asset types to be returned
 * @param {*} sortValue - Ascending or descending order based on name and date uploaded
 * @param {string} viewName - The type of view to be displayed(List or Grid)
 * @returns {function}
 * The object mimicking the metadata object, but with every action wrapped into the 'dispatch' call.
 * This action creator returns a function.
*/
export function getSearchProductItems(value,pageNo,maxItems, filter, sortOption,viewName){
  return dispatch => {
    let index, limit;
    if(pageNo===1){
      index = 0;
      limit = index+maxItems;
    }else{
      index = (pageNo*maxItems)-maxItems;
      limit = index+maxItems-1;
    }
    let selIndex;
    if(sessionStorage.AssetTabIndex){
      if(sessionStorage.AssetTabIndex===4){
        selIndex = 0;
      }else{
      selIndex = parseInt(sessionStorage.AssetTabIndex);
      }
    }else{
      selIndex = 0;
    }
      searchLibraryApi.getAssertsData(value,filter,index,limit, sortOption)
      .then(function (res) {
        res.body = {};
        res.body.index = index;
        res.body.limit = limit;
        res.body.pageNo = pageNo;
        res.body.pageLimit = maxItems;
        res.body.SearchValue = value;
        res.body.showSaveSearch = true;
        res.body.selectedIndex = selIndex;
        res.body.displayItemCount = maxItems;
        if(viewName){
          res.body.viewName = viewName;
        }else{
          res.body.viewName = 'grid-view';
        }
        fileUploadApi.get_token().then(function (success){
          let  token = JSON.parse(success.text).data.ticket;
          res.body.token = token;

          let tempAssetsData = res.results.map(function (obj) {
                if (obj.thumbnail) {
                  obj.url = obj.thumbnail['@value']+'&alf_ticket='+token;
                }
                else {
                  obj.url = '../../../images/default-thumbnail.gif';
                }

                obj.mimetype = '';
                obj.name = obj.name['@value'];
                if (obj.dateModified) {
                  obj.modifiedOn = obj.dateModified['@value'];
                }
                else {
                  obj.modifiedOn = new Date();
                }

                return obj;
          });

          res.body.items = tempAssetsData;
          dispatch({
            type: SEARCH_DISPLAY_ASSETS,
            data: res.body
          })
        })
       })
    }
  }


  export function getDifficultyLevels(){
    return dispatch => {
     // searchLibraryApi.fetch_savedSearch_data().then(function (data) {
      searchLibraryApi.difficultyLevelData().then(function (data) {
        dispatch({
          type: 'DIFFICULTY_LEVELS',
          data: data
        })
      })
  }
}

/** @function saveSearchValues -
 * This method is for saving a search value
 * @param {string} value - The search value to be saved
*/
export function saveSearchValues(value){

  return dispatch => {
     // searchLibraryApi.fetch_savedSearch_data().then(function (data) {
      searchLibraryApi.saveSearchValue(value).then(function (data) {
        /*dispatch({
          type: DISPLAY_ASSETS,
          data: JSON.parse(data.text)
        })*/
      })
  }
}

/** @function saveSearchValues -
 * This method is to determine whether to show select and cancel buttons
    under search library tab
 * @param {*} isSavedSearch - Boolean value to determine whether to show
 select and cancel buttons under search library tab
*/
export function searchLibButtonVisibility(isSavedSearch){
   return (dispatch) => {
        dispatch({
          type: SEARCH_BUTTON_VISIBILITY,
          isSavedSearch:{'isSavedSearch':isSavedSearch}
        })
  }
}

export function updateDifficultyLevel(difficultyLevelId){
    return {
        type : 'UPDATE_DIFFICULTY_LEVEL',
        data : difficultyLevelId
    }
}
/** @function deleteSavedSearch -
 * This method is to delete a saved search value
*/
export function deleteSavedSearch(){
  return (dispatch, getState) => {
    localforage.getItem('savedSearch', function (err, res) {
      let state = getState();
      let savedSearchData = state.savedSearchReducers[0].savedData
      let filteredData = [],deletedData=[];
       for(let i=0;i<res.length;i++){
        for(let j=0;j<savedSearchData.length;j++){
          if(res[i].id === savedSearchData[j].id){
            if(savedSearchData[j].checked===true){
              res[i].isChecked = true;
            }
          }
        }
       }
       for(let i=0;i<res.length;i++){
        if(res[i].isChecked===false){
          filteredData.push(res[i]);
        }else{
          deletedData.push(res[i]);
        }
       }
       dispatch({
            type: 'DELETE_CHECKED_SAVED_SEARCH_VALUE',
            data: deletedData
          })
       if(localforage.setItem('savedSearch',filteredData)){
       localforage.getItem('savedSearch', function (err, res) {
       dispatch(fetchSavedSearchData(DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS));
        });
      }
    });
  }
}


/** @function deleteSavedSearch -
 * This method is to run the search again with the value checked under saved
 search tab
*/
export function runSearch(){
    return (dispatch, getState) => {
    let state = getState();
      let savedSearchData = state.savedSearchReducers[0].savedData;
      savedSearchData.map(function (item){
          if(item.checked===true){
            document.querySelector('#addAnAssets .react-autosuggest__input').value = item.name;
            let prevValue = state.autoComplete[state.autoComplete.length-1];
          dispatch({
          type: AUTO_COMPLETE,
          data: prevValue.data,
          text: item.name,
          savedSearch: prevValue.savedSearch,
          lastThreeSearch: prevValue.lastThreeSearch
        });
        dispatch(getSearchProductItems(item.name,DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS,serviceUrls.searchBasedAll));
          }
      });
  }
}
